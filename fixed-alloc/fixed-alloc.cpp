// fixed-alloc.cpp : fixed blocks allocator
//

#include "pch.h"
#include <iostream>
#include <cstddef>
#include "fixed-alloc.h"
#include <assert.h>
#include <cstring>

////////////////////////////////////////////////////////////////////////
//
// FixedAlloc::Chunk
//
////////////////////////////////////////////////////////////////////////
FixedAlloc::Chunk::Chunk(void* buffer_, size_t memorySize_, size_t blockSize_)
  : buffer(static_cast<data_t*>(buffer_))
  , chunkSize(memorySize_)
  , blockSize(blockSize_)
  , maxBlocks( 0 )
  , firstFree( 0 )
{
  memset(buffer, 0, chunkSize);

  // assert( chunkSize == blockSize )
  init();
}

/** Initializes linked list of free blocks, first byte of any free block holds index of the next free block. */
void FixedAlloc::Chunk::init() {
  firstFree = 0;
  // assert( blockSize > 0 )
  // assert( chunkSize > 0 )
  // assert( buffer != nullprt )
  maxBlocks = chunkSize / blockSize;
  // assert( maxBlocks <= MAX_BLOCKS )
  ptrdiff_t itemPos = maxBlocks;
  
  for (; --itemPos >= 0; ) {
    data_t *elem = buffer + itemPos * blockSize;

    ptrdiff_t next = itemPos < 255 ? itemPos + 1 : 0;
    elem[0] = static_cast<data_t>(next);
  }
}

/** Returns an offset for a block under position 'index' */
size_t FixedAlloc::Chunk::getPosition(data_t index) const {
  return index * blockSize;
}

/** Tries to allocate new block from a chunk.
 * @returns - nullptr - cannot allocate, pointer to a block - block was allocated successfully.
 */
void* FixedAlloc::Chunk::doAllocate() {
  if (!buffer)
    return nullptr;
  if (isFull())
    return nullptr;

  data_t* nextFree = buffer + getPosition(firstFree);
  void* allocated = nextFree;
  if (0 == *nextFree) {
    // end of a chunk reached
    firstFree = (unsigned)this->maxBlocks;
  }
  else {
    firstFree = *nextFree;
  }
  return allocated;
}

/** Defines whether the pointer belongs to the chunk object.
 *  @return true - pointer belongs to the chunk, false - doesn't belong to it.
 */
bool FixedAlloc::Chunk::isOwnBlock(void* p) const {
  if (nullptr == p)
    return false;

  //!!
  data_t *block = static_cast<data_t*>(p);
  // check if the pointer passed chunk belongs to the chunk.
  ptrdiff_t distance = block - buffer;

  // huge speed-up: 3.9 -> 2.4
  //bool good = distance >= 0 && distance / blockSize < maxBlocks;
  bool good = distance >= 0 
    && (size_t)distance < maxBlocks * blockSize;
  return good;
}

/** Tries to deallocate previously allocated block */
void  FixedAlloc::Chunk::doFree(void* p) {
  if (nullptr == p)
    return; // throw something

  data_t *block = static_cast<data_t*>(p);
  // check if the pointer passed chunk belongs to the chunk.
  ptrdiff_t distanceBytes = block - buffer;
  ptrdiff_t distanceBlocks = distanceBytes / blockSize;
  // todo: check if pointer is good (points on a boundary between objects).

  if (distanceBytes >= 0 && distanceBlocks < static_cast<ptrdiff_t>(maxBlocks)) {
    // good block, deallocate
    // size_t pos = getPosition(distance); // error here

    if (firstFree > static_cast<unsigned>(distanceBlocks)) {
      // add to head of free blocks list
      freeBlockPushFront(distanceBytes, distanceBlocks);
    } else {
      // iterate free elements and insert a link to this element to previous free, and next free element to this element.
      freeBlockInsertRandom(distanceBlocks, block);
      // todo: add to the end of array
    }

  }
  else {
    // todo: throw something!
  }
}

void FixedAlloc::Chunk::freeBlockInsertRandom(const ptrdiff_t &distanceBlocks, unsigned char * block)
{
  for (size_t index = firstFree; index < maxBlocks; ) {
    /*
    // firstFree - free[0]
    // cur[0] - free[1]
    */
    data_t* cur = &buffer[getPosition((data_t)index)];
    ptrdiff_t nextBlockIndex = cur[0] == 0 ? maxBlocks : cur[0];

    if ((ptrdiff_t)index < distanceBlocks && distanceBlocks < nextBlockIndex) {
      // insert between them
      data_t nextVal = cur[0];
      cur[0] = static_cast<data_t>(distanceBlocks);
      block[0] = nextVal;
      break;
    }
    else {
      // move to the next free block
      index = nextBlockIndex;
      continue;
    }
  }
}

void FixedAlloc::Chunk::freeBlockPushFront(const ptrdiff_t &distanceBytes, const ptrdiff_t &distanceBlocks)
{
  size_t pos = distanceBytes;
  data_t* markFree = &buffer[pos];
  data_t* firstFreeEl = &buffer[getPosition(firstFree)];

  if (firstFree <= maxBlocks) {
    if (firstFree == maxBlocks)
      markFree[0] = (data_t)0;
    else
      markFree[0] = (data_t)firstFree;
  }
  firstFree = static_cast<unsigned>(distanceBlocks);
}

// How many chunks in header
FixedAlloc::ChunkHeader::ChunkHeader(void* memoryBlock, size_t memorySize_, size_t blockSize_)
  : magic(0xdeadbeaf)
  , version(1)
  , chunkObjectSize(sizeof(Chunk))
  , rawSize(0)
  , memorySize(memorySize_)
  , blockSize(blockSize_)
{
  const size_t chunkObjectSize = sizeof(Chunk);
  size_t chunkBufferLength = chunkBufferSize();

  size_t numChunks = howManyChunksNeeded(memorySize);
  size_t chunkHeaderRawSize = chunkObjectSize * numChunks;
  size_t ownSize = sizeof(ChunkHeader);

  this->chunkPtr = (Chunk**)((Chunk::data_t*)memoryBlock + ownSize); // point to hidden array of pointers
  if (numChunks > 0) {
    // add array of chunk pointers here
    ownSize += (numChunks) * sizeof(FixedAlloc::Chunk*);
  }

  size_t lostBlockSize = ownSize + chunkHeaderRawSize;
  // assert( memorySize >= lostBlockSize )
  size_t actualNumChunks = howManyChunksNeeded(memorySize - lostBlockSize);

  lostBlockSize = ownSize + actualNumChunks * chunkObjectSize;
  this->numChunks = actualNumChunks;
  this->rawSize = static_cast<unsigned>(lostBlockSize);

  // allocate array of chunk objects
# if 0
  {
    FixedAlloc::Chunk::data_t *buffer = static_cast<FixedAlloc::Chunk::data_t*>(memoryBlock);
    FixedAlloc::Chunk::data_t *chunksHeader = buffer + ownSize; // todo: does it need alignment? points to header's chunks array
    FixedAlloc::Chunk::data_t *storage = buffer + this->rawSize; // points to chunks data memory

    size_t memoryUsed = lostBlockSize;
    Chunk* lastAllocated = nullptr;
    for (size_t it = 0; it < this->numChunks; it++) {
      size_t offset = sizeof Chunk * it;
      void* nextChunkObject = chunksHeader + offset;
      void* nextChunkBlock = storage + it * chunkBufferLength;

      // initialize them with actual memory pools
      Chunk* allocated = new(nextChunkObject) FixedAlloc::Chunk(nextChunkBlock, chunkBufferLength, blockSize);
      lastAllocated = allocated;

      memoryUsed += chunkBufferLength;
    }

    this->chunks = reinterpret_cast<FixedAlloc::Chunk*>(chunksHeader);
  }
# endif // 0

  // dev-chunks: allocate (chunk header, chunk data) pairs and save chunk pointers to chunkPtr[]
  {
    FixedAlloc::Chunk::data_t *buffer = static_cast<FixedAlloc::Chunk::data_t*>(memoryBlock);
    FixedAlloc::Chunk::data_t *chunksHeader = buffer + ownSize; // todo: does it need alignment? points to header's chunks array
    FixedAlloc::Chunk::data_t *storage = buffer + this->rawSize; // points to chunks data memory

    size_t memoryUsed = lostBlockSize;
    Chunk* lastAllocated = nullptr;
    for (size_t it = 0; it < this->numChunks; it++) {
      size_t offset = (sizeof(Chunk) + chunkBufferLength) * it;
      Chunk::data_t* nextChunkObject = chunksHeader + offset;
      Chunk::data_t* nextChunkBlock = ((Chunk::data_t*)nextChunkObject) + sizeof(Chunk);

      // initialize them with actual memory pools
      Chunk* allocated = new(nextChunkObject) FixedAlloc::Chunk(nextChunkBlock, chunkBufferLength, blockSize);
      lastAllocated = allocated;
      this->chunkPtr[it] = allocated;

      memoryUsed += sizeof(Chunk) * chunkBufferLength;
    }

    Chunk** ptrs = &this->chunkPtr[0];
  }
}

size_t FixedAlloc::ChunkHeader::chunkBufferSize() const {
  return Chunk::MAX_BLOCKS * blockSize;
}
size_t FixedAlloc::ChunkHeader::howManyChunksNeeded(size_t memoryBlockSize) const {
  // assert( blockSize > 0 )
  return memoryBlockSize / chunkBufferSize();
}

/** Searches for the first free chunk, returns its index. Uses saved previous search result to perform faster.
 *  If nothing was found, returns -1. */
ptrdiff_t FixedAlloc::ChunkHeader::findFirstFree() const {
  if (numChunks > 0 && chunkPtr[0]) {
    size_t start = 0, it = 0;
    if (lastFirstFree > -1) {
      start = lastFirstFree;
      Chunk const& cur = *chunkPtr[start];
      if (!cur.isFull()) {
        return start;
      }
    }
    //for (it = start; it < numChunks + start; it++) {
    // it == numChunks - no memory - breaks random test
    for (it = start; it < numChunks; it++) {
      auto index = it % numChunks;
      Chunk const& cur = *chunkPtr[index]; // cycle here: [start -> end -> 0 -> start -1]

      if (!cur.isFull()) {
        return index;
      }
    }
  }
  return -1;
}

/** Returns index of a chunk to which the pointer belongs to, -1 if nothing was found. */
ptrdiff_t FixedAlloc::ChunkHeader::findOwnChunk(void* block) const {
  bool random = true;
  // uses for loop, leanear search
  if (random) {
    auto withRaw = findOwnChunkRaw(block);
    return withRaw;
  }
  else {
    auto withFor = findOwnChunkFor(block);
    return withFor;
  }

  //assert(withRaw == withFor);
  // randow access
}

/** Returns index of a chunk to which the pointer belongs to, -1 if nothing was found.
 *  Uses random access, no for loop by chunks, immediate. */
ptrdiff_t FixedAlloc::ChunkHeader::findOwnChunkRaw(void* block) const {
  if (block && numChunks && chunkPtr[0]) {
    Chunk::data_t* buffer = (Chunk::data_t*)chunkPtr[0];

    if (buffer) {
      auto actual = buffer;

      auto diff = (static_cast<Chunk::data_t*>(block) - actual) / (sizeof(Chunk) + chunkBufferSize());
      if (diff >= 0 && diff < numChunks) { // +1 for last chunk that can be smaller than others
        // todo: handle last chunk (numBlocks < MAX_BLOCKS) here
        Chunk const& cur = *chunkPtr[diff];

        if (cur.isOwnBlock(block)) {
          return diff;
        }
      }
    }
  }
  return -1;
}

/** Returns index of a chunk to which the pointer belongs to, -1 if nothing was found.
 * Uses for loop by chunks, leaner search */
ptrdiff_t FixedAlloc::ChunkHeader::findOwnChunkFor(void* block) const {
  if (block != nullptr && chunkPtr[0] && numChunks > 0) {
    // find chunk with free space
    size_t start = lastOwnChunk > -1 ? lastOwnChunk : 0;
    //size_t start = 0;
    for (size_t it = start; it < numChunks + start; it++) {
      auto index = it % numChunks;
      Chunk const& cur = *chunkPtr[index];

      if (cur.isOwnBlock(block)) {
        return index;
      }
    }
  }
  return -1;
}

/** No checks inside, check all in client code. */
void FixedAlloc::ChunkHeader::setLastFirstFree(ptrdiff_t v) {
  lastFirstFree = v;
}
/** No checks inside, check all in client code. */
void FixedAlloc::ChunkHeader::setLastOwnChunk(ptrdiff_t v) {
  lastOwnChunk = v;
}


////////////////////////////////////////////////////////////////////////
//
// FixedAlloc implementation
//
////////////////////////////////////////////////////////////////////////
FixedAlloc::FixedAlloc(void* memoryPool_, size_t memorySize_, size_t blockSize_)
  : memoryPool( memoryPool_)
  , buffer(memoryPool_)
  , bufferSize(memorySize_)
  , blockSize(blockSize_)
  , alignedSize(blockSize_)
{
  if (buffer == nullptr || bufferSize == 0 || blockSize == 0) {
    assert(!"FixedAlloc: invalid arguments passed");
    throw std::logic_error("FixedAlloc: invalid arguments passed");
  }
  memset(buffer, 0, bufferSize);

  // TODO: alignment feature should be implemented here for uneven block sizes.
  if (blockSize >= 3 && blockSize % 2 == 1) {
    size_t ptrSize = sizeof(ptrdiff_t);
    // align using
    switch (ptrSize) {
    case 4: //blockSize = ((blockSize / ptrSize) + 1) * ptrSize; break;
    case 8: alignedSize = ((blockSize / ptrSize) + 1) * ptrSize; break;
    default: throw std::logic_error("Unsupported system pointer width");
    }
  }
  chunkSize = alignedSize * 256;
  if (chunkSize > bufferSize)
    chunkSize = bufferSize;

  numChunks = bufferSize / chunkSize > 0 ? chunkSize : 1;
  maxInChunk = chunkSize / alignedSize;


  /* Calculate available number of chunks for the memory block.
     Fill memory block with the header object: header (magic, number of chunks, actual memory block size without header)
     Create with inplace new and construct all Chunk objects needed to handle the memory block left.
  */

  this->header = new(memoryPool_) ChunkHeader(memoryPool_, memorySize_, alignedSize);
  ChunkHeader& r = *this->header;
}

void* FixedAlloc::allocate() {
  if (header && header->curChunk) {
    auto res = header->curChunk->doAllocate();
    if (res) {
      auto& c = getCounters();
      c.inc(c.alloc0);

      return res;
    }
    else {
      header->curChunk = nullptr;
    }
  }
  if (header && header->numChunks > 0 && header->chunkPtr[0]) {
    /// !!! 
    if (header->lastFirstFree > -1) {
      Chunk* p = header->chunkPtr[header->lastFirstFree];
      if (p && ! p->isFull()) {
        header->curChunk = p;
        auto res = p->doAllocate();
        if (res) {
          auto& c = getCounters();
          c.inc(c.alloc1);
          return res;
        }
      }
    }
    /// !!!
    // find chunk with free space
    ptrdiff_t freeIndex = header->findFirstFree();

    if (freeIndex > -1) {
      header->setLastFirstFree(freeIndex);

      auto& c = getCounters();
      c.inc(c.alloc2);

      Chunk& cur = *header->chunkPtr[freeIndex];
      return cur.doAllocate();
    }
  }
  return nullptr;
}

void  FixedAlloc::free(void* block) {
  if (nullptr == block)
    return;
  // find chunk and delete block from it.
  // if chunk is empty, delete chunk also
  if (header && header->numChunks > 0 && header->chunkPtr[0]) {
    // find chunk with free space
    ptrdiff_t indexFound = header->findOwnChunk(block);

    if (indexFound > -1) {
      Chunk& cur = *header->chunkPtr[indexFound];
      cur.doFree(block);

      header->setLastOwnChunk(indexFound);
      if ( header->lastFirstFree > indexFound )
        header->setLastFirstFree(indexFound);
      return;
    }
    // todo: throw something
  }
}

bool  FixedAlloc::isBad() const {
  return buffer == nullptr || bufferSize == 0 || blockSize == 0 || blockSize > bufferSize;
}

FixedAlloc::Counters& FixedAlloc::getCounters() {
  static Counters c;
  return c;
}

#include "pch.h"
#include <iostream>
#include <vector>
#include <cerrno>
#include <assert.h>
#include <chrono>
#include <memory>
#include <cstring>

#include "fixed-alloc.h"

using namespace std;
using namespace std::chrono;

const char* usage() {
  const char* message = 
    "fixed-alloc test app \n"
    "Usage: fixed-alloc memory-size block-size allocateBlocks repeatTest\n";
  return message;
}
void usage(std::ostream& os) {
  const char* message = usage();
  if (message)
    os << message << std::endl;
}

template <class Value>
class AllocSubject {
public:
  AllocSubject(Value const& value_ = 0)
    : value(value_)
  {
    counter = AllocSubject::getInstancesCreated( true );
  }
  ~AllocSubject() {
    reset();
    AllocSubject::getInstancesDeleted( true );
  }

  static size_t& getInstancesCreated( bool inc = false) {
    static size_t instances = 0;
    if (inc)
      ++instances;
    return instances;
  }
  static size_t& getInstancesDeleted( bool inc = false ) {
    static size_t instances = 0;
    if (inc)
      ++instances;
    return instances;
  }

  size_t                  getCounter() const { return counter; }
  Value const&            getValue() const { return value; }
  AllocSubject&           setValue(Value const& newVal) { value = newVal; }
  void                    reset() { counter = 22; }

private:
  size_t                  counter;
  Value                   value;
};

template <class TestData>
class TestSingleFixedAlloc {
public:
public:
  TestSingleFixedAlloc(void* memoryPool_, size_t memorySize_, size_t blockSize_)
    : memoryPool(memoryPool_)
    , memorySize(memorySize_)
    , blockSize(blockSize_)
    , alloc( memoryPool_, memorySize_, blockSize_ )
  {
  }

  int startTest(size_t toAllocate);
  int finishTest(const char* method = "same-order");
  void beSilent(bool v) { silent = v; }

  int runSingle(size_t numAlloc = 3, const char* method = "same-order", bool silent = false);

private:
  int finishSameOrder();
  int finishRandomly();

private:
  void* memoryPool;
  size_t memorySize;
  size_t blockSize;

  FixedAlloc alloc;
  std::vector<void*> pointers;
  bool silent = false;
};


template <class TestData>
int TestSingleFixedAlloc<TestData>::startTest(size_t toAllocate) {
  if (nullptr == memoryPool || 0 == memorySize)
    return -1;

  using namespace std;
  const size_t blockSize = sizeof(TestData);
  pointers.clear();
  pointers.resize(toAllocate);

  size_t countEmpty = 0;
  // allocate
  for (size_t it = 0; it < toAllocate && it < pointers.size(); it++) {
    void* allocated = alloc.allocate();
    pointers[it] = allocated;

    if (allocated) {
      // construct object
      TestData* pObject = new(allocated) TestData(it + 1000000);

      (void)pObject;
    }
    else {
      countEmpty++;
    }
  }

  return pointers.size() > 0 ? 0 : -1;
}

template <class TestData>
int TestSingleFixedAlloc<TestData>::finishTest(const char* method) {
  if (strcmp(method, "random") == 0)
    return this->finishRandomly();
  // all other cases:
  //if (strcmp(method, "same-order") == 0)
  return this->finishSameOrder();
}

template <class TestData>
int TestSingleFixedAlloc<TestData>::finishSameOrder() {
  for (size_t it = pointers.size(); it > 0; it--) {
    auto& raw = pointers[it - 1];
    if (!raw)
      continue;

    TestData* pObject = static_cast<TestData*>(raw);
    // destruct an object
    pObject->TestData::~TestData();
    pObject = nullptr;

    alloc.free(raw);
    raw = nullptr;
  }

  pointers.clear();

  if (!silent) {
    cout << "    # test complete, instances created: " << TestData::getInstancesCreated( false )
      << ", instances deleted: " << TestData::getInstancesDeleted( false )
      << endl;
  }

  return 0;
}

template <class TestData>
int TestSingleFixedAlloc<TestData>::runSingle(size_t numAlloc, const char* method, bool silent) {
  const size_t blockSize = sizeof(TestData);
  auto& tester = *this;

  //TestSingleFixedAlloc<TestData> tester(memoryPool, memorySize, blockSize);
  tester.beSilent(silent);

  steady_clock::time_point tp0 = steady_clock::now();

  int startRes = tester.startTest(numAlloc);

  steady_clock::time_point tp1 = steady_clock::now();
  duration<double> time_span = duration_cast<duration<double>>(tp1 - tp0);
  cout << "    # Allocation phase: " << method << ", Elapsed: " << time_span.count() << " seconds." << endl;

  int finishRes = tester.finishTest(method);

  steady_clock::time_point tp2 = steady_clock::now();
  time_span = duration_cast<duration<double>>(tp2 - tp1);
  cout << "    # Deallocation phase: " << method << ", Elapsed: " << time_span.count() << " seconds." << endl;

  TestData::getInstancesCreated(false) = 0;
  TestData::getInstancesDeleted(false) = 0;

  int totalRes = startRes == 0 && finishRes == 0 ? 0 : -1;
  return totalRes;
}

#include <algorithm>
#include <random>
#include <chrono>       // std::chrono::system_clock

template <class TestData>
int TestSingleFixedAlloc<TestData>::finishRandomly() {
  // shuffle pointers array
  auto seed = std::chrono::system_clock::now().time_since_epoch().count();
  shuffle(pointers.begin(), pointers.end(), std::default_random_engine((unsigned)seed));

  // call finishSameOrder
  return finishSameOrder();
}

template <typename TestData>
int testAlloc(void* memoryPool, size_t memorySize, size_t numAlloc = 3, const char* method = "same-order", bool silent = false) {
  //typedef AllocSubject<unsigned> TestData;
  const size_t blockSize = sizeof(TestData);
  TestSingleFixedAlloc<TestData> tester(memoryPool, memorySize, blockSize);

  if (0 == strcmp("both", method)) {
    array<const char*,2> methods{ "same-order", "random" };

    int res = -1;
    for (size_t it = 0; it < methods.max_size(); it++) {
      const char* method = methods[it];
      steady_clock::time_point tp0 = steady_clock::now();

      res = tester.runSingle(numAlloc, method, silent);

      steady_clock::time_point tp1 = steady_clock::now();
      duration<double> time_span = duration_cast<duration<double>>(tp1 - tp0);
      cout << "Method: " << method << ", Elapsed: " << time_span.count() << " seconds." << endl << endl;
    }
    return res;
  }
  else {
    int totalRes = tester.runSingle(numAlloc, method, silent);

    return totalRes;
  }
}

template <class TestData>
int testMallocFree(size_t memorySize, size_t blocksToAllocate, const char* method, bool silent) {
  // typedef AllocSubject<unsigned> TestData;
  const size_t blockSize = sizeof(TestData);

  vector<void*> pointers; pointers.reserve(blocksToAllocate);

  for (size_t it = 0; it < blocksToAllocate; it++) {
    void* ptr = malloc(blockSize);
    pointers.push_back(ptr);

    new (ptr) TestData();
  }

  cout << "Malloc: allocated " << blocksToAllocate << " records" << endl;
  if (method && 0 == strstr(method, "random")) {
    // shuffle pointers array
    auto  seed = std::chrono::system_clock::now().time_since_epoch().count();
    shuffle(pointers.begin(), pointers.end(), std::default_random_engine((unsigned)seed));
  }

  for (size_t it = blocksToAllocate; it > 0; it--) {
    void* ptr = pointers[it - 1];
    if (ptr) {
      static_cast<TestData*>(ptr)->TestData::~TestData();

      free(ptr);
      pointers[it - 1] = nullptr;
    }
  }

  cout << "    # test complete, instances created: " << TestData::getInstancesCreated(false)
    << ", instances deleted: " << TestData::getInstancesDeleted(false)
    << endl;

  TestData::getInstancesCreated(false) = 0;
  TestData::getInstancesDeleted(false) = 0;
  return 0;
}

template<class TestData>
int testMain( void* memoryBlock, size_t memorySize, const char* testName, const char* method ) {
  const size_t blockSize = sizeof(TestData);
  size_t blocksToAllocate = memorySize / blockSize;
  unsigned repeatTest = 1;
  FixedAlloc alloc(memoryBlock, memorySize);

  if ( ! method ) method = "same-order";
  if (!testName) testName = "unknown";

  cout << "testMain<" << testName << ">" << endl;

  bool doTestSmallCount = false;
  bool doTestLargeCount = false;
  bool doTestSmallCountRandomFree = true;

  if (doTestSmallCount) {
    cout << "simpletestAlloc(memSize: " << memorySize
      << ", type: " << testName
      << ", type sizeof " << sizeof(TestData) << ")";
    auto testResult = testAlloc<TestData>(memoryBlock, memorySize, blocksToAllocate, "same-order");
    cout << "Test result: " << testResult << endl;
  }

  if (doTestLargeCount) {
    blocksToAllocate = 3 * 256 - 3;
    cout << "simpletestAlloc(memSize: " << memorySize
      << ", type: " << testName
      << ", type sizeof " << sizeof(TestData)
      << ", blocksToAllocate: " << blocksToAllocate
      << ")";
    auto testResult = testAlloc<TestData>(memoryBlock, memorySize, blocksToAllocate, "same-order");
    cout << "Test result: " << testResult << endl;
  }

  bool doTestMallocFree = true;
  if (doTestMallocFree) {
    cout << "testMallocFree (memSize: " << memorySize
      << ", type: " << testName
      << ", type sizeof " << sizeof(TestData)
      << ", blocksToAllocate: " << blocksToAllocate
      << ")" << endl;

    steady_clock::time_point tp0 = steady_clock::now();

    for (unsigned i = 0; i < repeatTest; i++) {
      bool silent = true;
      if ((i + 1) % 1000 == 0 || i == repeatTest - 1)
        silent = false;

      //const char* method = "same-order";
      auto testResult = testMallocFree<TestData>(memorySize, blocksToAllocate, method, silent);
      //cout << "Test result: " << testResult << endl;
    }

    steady_clock::time_point tp1 = steady_clock::now();
    duration<double> time_span = duration_cast<duration<double>>(tp1 - tp0);
    cout << "Method: " << method << ", Elapsed: " << time_span.count() << " seconds." << endl << endl;
  }

  if (doTestSmallCountRandomFree) {
    const char* ownMethod = "both";

    cout << "testAlloc " << ownMethod << "(memSize: " << memorySize
      << ", type: " << testName
      << ", type sizeof " << sizeof(TestData)
      << ", blocksToAllocate: " << blocksToAllocate
      << ")" << endl;

    steady_clock::time_point tp0 = steady_clock::now();

    for (unsigned i = 0; i < repeatTest; i++) {
      bool silent = true;
      if ((i + 1) % 1000 == 0 || i == repeatTest - 1)
        silent = false;

      //const char* method = "same-order";
      auto testResult = testAlloc<TestData>(memoryBlock, memorySize, blocksToAllocate, ownMethod, silent);
      //cout << "Test result: " << testResult << endl;
    }

    steady_clock::time_point tp1 = steady_clock::now();
    duration<double> time_span = duration_cast<duration<double>>(tp1 - tp0);
    cout << "Method: " << ownMethod << ", Elapsed: " << time_span.count() << " seconds." << endl;
  }

  std::cout << "End" << std::endl;

  return 0;
}

template <std::size_t N>
struct SNumbers {
  std::array<size_t, N> values;
  SNumbers(size_t v) {
    values[0] = v;
  }
};

int main(int argc, char* argv[])
{
	std::cout << "Hello, fixed_alloc test!\n";
  usage(std::cout);

  size_t blockSize = 16;
  size_t memorySize = 3 * 4096;
  size_t blocksToAllocate = 756;
  unsigned repeatTest = 1;
  const char* method = "same-order";

  cout << "sizeof size_t: " << sizeof(size_t) << endl;
  cout << "sizeof ptrdiff_t: " << sizeof(ptrdiff_t) << endl;
  if (argc >= 3) {
    ptrdiff_t cfg[2]{ atoll(argv[1]), atoll(argv[2]) };

    if (cfg[0] > 0) memorySize = cfg[0];
    if (cfg[1] > 0) blockSize = cfg[1];
  }
  if (argc >= 4 && atoll(argv[3]) > 0) {
    blocksToAllocate = atoll(argv[3]);
  }
  if (argc >= 5 && atoll(argv[4]) > 0) {
    repeatTest = atoi(argv[4]);
  }
  if (argc >= 6  ) {
    method = argv[5];
  }

  blocksToAllocate = (memorySize - 256) / blockSize;

  std::cout << "Memory size: " << memorySize
    << ", blockSize: " << blockSize
    << ", blocksToAllocate: " << blocksToAllocate 
    << ", repeatTest: " << repeatTest
    << ", method: " << method
    << endl;

  std::unique_ptr<unsigned char> memoryPool( new unsigned char[memorySize] );

  // todo: test it with blockSize: [1, 2, 3, 4, 5, 7, 8, 13, 16, 24, 33, 133, 1024]

  typedef AllocSubject<unsigned> TestData;

  int res = 0;
  res = testMain<TestData>(memoryPool.get(), memorySize, "AllocSubject<unsigned>", method);
  res = testMain<AllocSubject<size_t>>(memoryPool.get(), memorySize, "AllocSubject<size_t>", method);

  res = testMain<AllocSubject<SNumbers<10>> >(memoryPool.get(), memorySize, "AllocSubject<SNumbers<10>>", method);

  return 0;
}

/*
Develop class providing memory management interface that allocates blocks 
	of a certain fixed size from a pre-allocated continuous region of memory.

Interface description:
- Constructor should accept 3 parameters: pointer to pre-allocated memory area,
	memory size and block size
- void* allocate();
- void free(void* p);

Implementation should not use dynamic memory allocation at all 
(�inplace new� only) and there�s no need to develop thread safe solution 
(solution should work in single threaded application only).
*/
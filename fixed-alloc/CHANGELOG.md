# fixed-alloc test project

## Original task
2019-05-03

Develop class providing memory management interface that allocates blocks of a certain fixed 
size from a pre-allocated continuous region of memory.

Interface description:
- Constructor should accept 3 parameters: pointer to pre-allocated memory area, memory size 
and block size.
- void* allocate();
- void free(void* p);

Implementation should not use dynamic memory allocation at all (“inplace new” only) and 
there’s no need to develop thread-safe solution (solution should work in single threaded 
application only).


## Changelog
## todos:
Allow the last chunk to be less than 256 elements long. Now if the allocated buffer is greater than header + full chunks, 
 space is not used.

### 2019-05-09
#### Usage: fixed-alloc [memory-block]
#### Tests
Tests for three classes added: AllocSubject<unsigned>, AllocSubject<size_t>, AllocSubject<SNumbers<10> >.

TDLR: malloc is faster than FixedAlloc + random up to 3 times: [25%, 200%], but uses up to 
3 times more memory.

#### Test results for 2 000 000 000 block for ReleaseOpt (/O2):

	* testMallocFree: allocates and deallocates N blocks (N calculated from 
		memoryBlocks and blockSize).
	* testAlloc: allocates and deallocates blocks using FixedAlloc.
	* testAlloc method = same-order: allocates blocks and then deallocates them in 
		reverse order (best case for FixedAlloc).
	* testAlloc method = random: allocates blocks, saves pointers allocated, shuffles 
		array with them and then deallocates them randomly (worst case for FixedAlloc).

```
Memory size: 2000000000, blockSize: 16, blocksToAllocate: 124999984, repeatTest: 1, method: same-order
testMain<AllocSubject<unsigned>>
testMallocFree (memSize: 2000000000, type: AllocSubject<unsigned>, type sizeof 16, blocksToAllocate: 125000000)
Malloc: allocated 125000000 records
    # test complete, instances created: 125000000, instances deleted: 125000000
Method: same-order, Elapsed: 102.75 seconds.

testAlloc both(memSize: 2000000000, type: AllocSubject<unsigned>, type sizeof 16, blocksToAllocate: 125000000)
    # Allocation phase: same-order, Elapsed: 1.26841 seconds.
    # test complete, instances created: 123535104, instances deleted: 123535104
    # Deallocation phase: same-order, Elapsed: 3.43127 seconds.
Method: same-order, Elapsed: 4.6997 seconds.

    # Allocation phase: random, Elapsed: 0.918413 seconds.
    # test complete, instances created: 123535104, instances deleted: 123535104
    # Deallocation phase: random, Elapsed: 123.178 seconds.
Method: random, Elapsed: 124.096 seconds.

Method: both, Elapsed: 129.399 seconds.
End
testMain<AllocSubject<size_t>>
testMallocFree (memSize: 2000000000, type: AllocSubject<size_t>, type sizeof 16, blocksToAllocate: 125000000)
Malloc: allocated 125000000 records
    # test complete, instances created: 125000000, instances deleted: 125000000
Method: same-order, Elapsed: 108.2 seconds.

testAlloc both(memSize: 2000000000, type: AllocSubject<size_t>, type sizeof 16, blocksToAllocate: 125000000)
    # Allocation phase: same-order, Elapsed: 1.28223 seconds.
    # test complete, instances created: 123535104, instances deleted: 123535104
    # Deallocation phase: same-order, Elapsed: 3.44485 seconds.
Method: same-order, Elapsed: 4.72709 seconds.

    # Allocation phase: random, Elapsed: 0.918821 seconds.
    # test complete, instances created: 123535104, instances deleted: 123535104
    # Deallocation phase: random, Elapsed: 122.809 seconds.
Method: random, Elapsed: 123.728 seconds.

Method: both, Elapsed: 129.068 seconds.
End
testMain<AllocSubject<SNumbers<10>>>
testMallocFree (memSize: 2000000000, type: AllocSubject<SNumbers<10>>, type sizeof 88, blocksToAllocate: 22727272)
Malloc: allocated 22727272 records
    # test complete, instances created: 22727272, instances deleted: 22727272
Method: same-order, Elapsed: 18.1138 seconds.

testAlloc both(memSize: 2000000000, type: AllocSubject<SNumbers<10>>, type sizeof 88, blocksToAllocate: 22727272)
    # Allocation phase: same-order, Elapsed: 0.46446 seconds.
    # test complete, instances created: 22678784, instances deleted: 22678784
    # Deallocation phase: same-order, Elapsed: 0.647368 seconds.
Method: same-order, Elapsed: 1.11184 seconds.

    # Allocation phase: random, Elapsed: 0.401924 seconds.
    # test complete, instances created: 22678784, instances deleted: 22678784
    # Deallocation phase: random, Elapsed: 43.7377 seconds.
Method: random, Elapsed: 44.1397 seconds.

Method: both, Elapsed: 45.6055 seconds.
End
```


### 2019-05-08
- [+] profiling: 400 Mb block, 26 mln records sizeof: 16: 
	* malloc/free - 16 sec (1.7Gb max):
	* fixed-alloc/same-order - 2.27 sec (593 Mb);
	* fixed-alloc/same-order - 45.3 sec (593 Mb).
- [+] lastFirstFree bugs: moved alse when deallocating; cycle by chunks disabled when allocating, memory error.
- [+] findOwnChunk: raw  version without looping through chunks.
- [+] isOwnBlock: / replaced with *, speed-up 30%.
- [+] dev-chunks: Trying to reorganize chunks: chunk member fields should be allocated near the chunk's block.

### 2019-05-07
- [+] Оптимизация поиска на больших объемах:
    * findFirstFree() takes 30% of all time.
    * findOwnChunk() 98% of all time after firstFree optimization. Rewritten to use random access.
- [-] Сравнение с malloc/free проиграно на больших объемах от милиона записей в 10 и больше раз. 
Попытки исправить ни к чему пока не привели.

### 2019-05-06
- [+] Random free() implemented.
- [+] Allocation/deallocation of more than one chunk of blocks.
- [+] Allocation/deallocation of a single element was implemented.
- [+] ChunkHeader initialized, chunks created and initialized.
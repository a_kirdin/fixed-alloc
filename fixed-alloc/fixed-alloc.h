#pragma once
#ifndef fixedAlloc_h_included
#	define fixedAlloc_h_included

# include <array>

class FixedAlloc {
private:
  class Chunk {
    friend class fixedAlloc;
  public:
    typedef unsigned char data_t;
    enum EDefaults { MAX_BLOCKS  = 256 };
  public:
    Chunk(void* buffer_, size_t memorySize_, size_t blockSize_);
    void                  init();

    bool                  isFull() const;
    bool                  isOwnBlock(void* p) const;
    void*                 doAllocate();
    void                  doFree(void*);
    void freeBlockInsertRandom(const ptrdiff_t &distanceBlocks, unsigned char * block);
    void freeBlockPushFront(const ptrdiff_t &distanceBytes, const ptrdiff_t &distanceBlocks);
    size_t                getPosition(data_t index) const;
    data_t*               getBuffer() { return buffer; }
    size_t                getMaxBlocks() const { return maxBlocks; }
  private:
    data_t                *buffer;   //!< Memory block
    size_t                chunkSize; //!< Size of memory block
    size_t                blockSize; //!< Block size - size of a single element
    size_t                maxBlocks; //!< Max number of blocks that could be allocated (should be less than or equal to 256)
    unsigned              firstFree; //!< Position of first free block; if less than 256, there are free blocks, >= 256 - no free blocks
  };

  class ChunkHeader {
    friend class FixedAlloc;
  public:
                          ChunkHeader(void* memoryBlock, size_t memorySize_, size_t blockSize_);

    size_t                chunkBufferSize() const;
    size_t                howManyChunksNeeded(size_t memoryBlockSize) const;
    ptrdiff_t             findFirstFree() const;
    ptrdiff_t             findOwnChunk(void*) const;

  private:
    void                  setLastFirstFree(ptrdiff_t v);
    void                  setLastOwnChunk(ptrdiff_t v);

    ptrdiff_t             findOwnChunkFor(void* block) const;
    ptrdiff_t             findOwnChunkRaw(void* block) const;

  private:
    unsigned              magic;   // platform independent type should be used
    unsigned              version; // platform independent type should be used
    unsigned              chunkObjectSize;
    unsigned              rawSize; // chunk header raw size
    size_t                memorySize;
    size_t                blockSize;
    size_t                numChunks = 0;
    //FixedAlloc::Chunk*    chunks = nullptr;
    FixedAlloc::Chunk*    curChunk = nullptr;
    ptrdiff_t             lastFirstFree = -1; // !< Saves last result of findFirstFree()
    ptrdiff_t             lastOwnChunk = -1; // !< Saves last result of findOwnChunk()

    FixedAlloc::Chunk**   chunkPtr = nullptr; // chunk pointers array to save chunk headers near the chunk buffer.
  };

  class Counters {
  public:
    enum EIndex { alloc0, alloc1, alloc2, alloc3, max_elem };
    std::array<size_t, max_elem> data{};

    void inc(unsigned index) {
      if (data.max_size() > index)
        data[index]++;
    }
  };

public:
  FixedAlloc(void* memoryPool_, size_t memorySize_, size_t blockSize_ = 16);

  void*                   allocate();
  void                    free(void* block);
  bool                    isBad() const;

  static Counters&        getCounters();

private:
  FixedAlloc(FixedAlloc const&);
  FixedAlloc& operator = (FixedAlloc const&);
private:
  void*                   memoryPool;//!< Pointer to a memory block, owned by client code.
  void*                   buffer; //!< Pointer to memory block inside memoryPool which holds chunks with data.
  size_t                  bufferSize; //!< Buffer size
  size_t                  blockSize; //!< Requested block size
  size_t                  alignedSize = 0; //!< Aligned block size
  ChunkHeader*            header = nullptr;

  // chunks
  size_t                  numChunks = 1;
  size_t                  chunkSize;
  size_t                  maxInChunk = 256;
};

/** Returns true if a chunk is full; false - otherwise. */
inline bool FixedAlloc::Chunk::isFull() const {
  /*
  if (firstFree >= maxBlocks)
    return true;
  return false;
  */
  return (firstFree >= maxBlocks);
}

#endif // fixedAlloc_h_included